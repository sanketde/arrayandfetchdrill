
/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/


const usersAPI = 'https://jsonplaceholder.typicode.com/users';
const todosAPI = 'https://jsonplaceholder.typicode.com/todos';

// 1. Fetch all the users
const fetchUsers = fetch(usersAPI)
  .then(response => response.json())
  .catch(error => console.error(error));

fetchUsers.then((users)=>{
    console.log(users);
})  

// 2. Fetch all the todos
const fetchtodos = fetch(todosAPI)
.then(response => response.json())
.catch(error => console.error(error));

fetchtodos.then((todos)=>{
    console.log(todos);
})

// 3. Use the promise chain and fetch the users first and then the todos.

fetch(usersAPI)
.then(response =>response.json())
.then(users => {
    console.log(users);
})
.then(() => fetch(todosAPI))
.then((response)=> response.json())
.then((todos) => {
    console.log(todos);
})


// 4. Use the promise chain and fetch the users first and then all the details for each user.

fetch(usersAPI)
  .then(response => response.json())
  .then(users => {
    const userPromises = users.map(user =>
      fetch(`${usersAPI}/?id=${user.id}`)
        .then(response => response.json())
    );
    return Promise.all(userPromises);
  })
  .then(userDetails => {
    console.log(userDetails);
  })
  .catch(error => console.error(error));



// 5.  Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that tod
let id = {};
fetch(`${todosAPI}/?id=${1}`)
.then(response => {
    return response.json();
})
.then((todo)=>{
    id = todo[0].userId;

    return fetch(`${usersAPI}/?id=${id}`);
})
.then(response => response.json())
.then((user)=>{
    console.log(user)
})


